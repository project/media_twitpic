<?php

/**
 *  @file
 *  Create a Twitpic Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $twitpic = new MediaTwitpicStreamWrapper('twitpic://[photo-code]');
 */
class MediaTwitpicStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://www.twitpic.com/';

  function interpolateUrl() {
    return $this->base_url . $this->get_parameters();
  }

  function getTarget($f) {
    return FALSE;
  }


  function getExternalUrl() {
    // Return the direct image.
    return $this->base_url . 'show/full/' . $this->get_parameters() . '.jpg';
  }

  function getOriginalThumbnailPath() {
    return $this->getExternalUrl();
  }
  function getLocalThumbnailPath() {
    $local_path = 'public://twitpic/' . $this->get_parameters() . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($this->getOriginalThumbnailPath(), $local_path);
    }
    return $local_path;
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'image/jpeg';
  }


  function setUri($uri) {
    parent::setUri($uri);
  }

  protected function _parse_url($url) {
    $path = explode('://', $url);
    return $path[1];
  }
}
